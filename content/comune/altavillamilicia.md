---
ID_PROV: 82
ID_REG: 19
URLAlbo: http://www.comune.altavillamilicia.pa.it/mc/mc_p_ricerca.php?servizio=&sto=&pag=&x=&pag=&mittente=&oggetto=&tipo_atto=&data_dal=&data_al=&datap_dal=&datap_al=&ordin=
URLTelegram: ''
URLfacebook: ''
URLtwitter: ''
autoreNome: Matteo Scirè
autoreURL: https://twitter.com/matteoscire
date: '2018-06-22T14:14:06+02:00'
draft: false
feedRSS: http://feeds.feedburner.com/AlboPopAltavillaMilicia
nome: Altavilla Milicia
province: Palermo
regioni: Sicilia
slug: ''
tags: []
tipologie: [comune]
title: Altavilla Milicia (Sicilia)
---