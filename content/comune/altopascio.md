---
ID_PROV: 46
ID_REG: 9
URLAlbo: http://159.213.69.82:8080/
URLTelegram: https://telegram.me/AlboPretorioAltopascio
URLfacebook: ''
URLtwitter: ''
autoreNome: Maurizio Tallarico
autoreURL: https://twitter.com/n0c0d3
date: '2018-06-22T14:14:06+02:00'
draft: false
feedRSS: http://feeds.feedburner.com/AlboPopAltopascio
nome: Altopascio
province: Lucca
regioni: Toscana
slug: ''
tags: []
tipologie: [comune]
title: Altopascio (Toscana)
---