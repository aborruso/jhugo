---
ID_PROV: 57
ID_REG: 12
URLAlbo: http://www.comune.accumoli.ri.it/albo-pretorio/
URLTelegram: ''
URLfacebook: ''
URLtwitter: ''
autoreNome: Enrico Bergamini
autoreURL: https://twitter.com/bergaminienrico
date: '2018-06-22T14:14:06+02:00'
draft: false
feedRSS: http://feeds.feedburner.com/AlbopopAccumoli
nome: Accumoli
province: Rieti
regioni: Lazio
slug: ''
tags: [tci]
tipologie: [comune]
title: Accumoli (Lazio)
---