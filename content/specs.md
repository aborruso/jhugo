---
title: "Specifiche dei feed RSS per AlboPOP"
date: 2018-06-24T22:53:21+02:00
draft: false
type: "staticpage"
---

Gli scraper di AlboPOP producono un [feed RSS](/feedrss/) il cui formato standard segue delle
[specifiche precise](https://cyber.harvard.edu/rss/rss.html).
Quelle che seguono sono le regole condivise per produrre feed dal formato omogeneo, valido per tutti gli albi pretori di origine.

I tag e gli attributi elencati qui sono sono suddivisi in tre tipologie:

* obbligatori: devono essere sempre presenti e valorizzati come indicato;
* raccomandati: se l'informazione esiste alla fonte, dovrebbe essere opportunamente estratta e presente anche nel feed;
* facoltativi: altre informazioni che caratterizzano gli albi e potrebbero essere presenti nel feed.

Altri elementi e attributi previsti dalle specifiche o da namespace aggiuntivi sono possibili, ma da considerarsi facoltativi.
Gli URL devono essere tutti riferimenti *assoluti* (http:// ecc.).

# Intestazione XML

[OBBLIGATORIO] La prima riga del feed deve contenere l'indicazione della versione dell'xml e dell'encoding.

```xml
<?xml version="1.0" encoding="UTF-8"?>
```

# Tag rss

[OBBLIGATORIO] L'elemento radice deve essere il tag `rss` con l'indicazione della versione *e degli eventuali namespace*.

```xml
<rss
 xmlns:creativeCommons="http://cyber.law.harvard.edu/rss/creativeCommonsRssModule.html"
 xmlns:xhtml="http://www.w3.org/1999/xhtml"
 version="2.0">
[...]
</rss>
```

# Tag channel
[OBBLIGATORIO] L'elemento figlio di [rss](#rss) deve essere l'elemento `channel`, senza attributi.

```xml
<channel>[...]</channel>
```

## Tag title
[OBBLIGATORIO] Il titolo del feed deve essere nella forma `AlboPOP - [tipo pa] - [nome pa]`. Esempio:

```xml
<title>AlboPOP - Comune - Bagheria</title>
```

## Tag link
[OBBLIGATORIO] L'URL diretto al feed.

```xml
<link>[...]</link>
```

## Tag description
[OBBLIGATORIO] La descrizione del feed. Deve essere nella forma `*non ufficiale* RSS feed dell'Albo Pretorio di [tipo pa] [nome pa]`. Esempio:

```xml
<description>*non ufficiale* RSS feed dell'Albo Pretorio del Comune di Bagheria</description>
```

## Tag language
[OBBLIGATORIO] La lingua dei contenuti del feed in formato [ISO 639-1](https://en.wikipedia.org/wiki/ISO_639-1). Esempio:

```xml
<language>it</language>
```

## Tag pubDate
[OBBLIGATORIO] Data e orario dell'ultimo aggiornamento del feed (per esempio, dell'ultima esecuzione dello scraper), in formato conforme alle specifiche [RFC 822](https://www.w3.org/Protocols/rfc822/#z28). Esempio:

```xml
<pubDate>Tue, 10 Jul 2016 04:00:00 +0000</pubDate>
```

## Tag webMaster
[OBBLIGATORIO] Nome ed email del curatore del feed e/o autore dello scraper. Esempio:

```xml
<webMaster>john@smith.com (John Smith)</webMaster>
```

## Tag docs
[OBBLIGATORIO] URL al repository pubblico che contiene il codice dello scraper o alla pagina web che lo descrive (per esempio, http://albopop.it/[tipo pa]/[nome pa]/). Esempio:

```xml
<docs>http://albopop.it/comune/bagheria/</docs>
```

## Tag copyright
[RACCOMANDATO] L'indicazione del copyright dei contenuti del feed, nella forma `Copyright [anno] [nome pa]`, nel caso di copyright esplicito della fonte. Alternativo al tag [creativeCommons:license](#channel-cc). Esempio:

```xml
<copyright>Copyright 2016 Comune di Bagheria</copyright>
```

## Tag creativeCommons:license
[RACCOMANDATO]  L'indicazione del copyleft dei contenuti del feed, in mancanza di un copyright esplicito della fonte. Alternativo al tag [copyright](#channel-copyright). Esempio:

```xml
<creativeCommons:license>http://creativecommons.org/licenses/by/4.0/</creativeCommons:license>
```

## Tag xhtml:meta
[OBBLIGATORIO] Necessario per evitare l'indicizzazione da motore di ricerca delle pagine linkate nel feed.

```xml
<xhtml:meta name="robots" content="noindex" />
```

## Tag channel category
Devono essere due (type e name) o più e contenere il maggior numero possibile delle seguenti informazioni, suddivise in obbligatorie (O), raccomandate \(R\), facoltative (F):

* nazione pa \(R\),
* regione pa \(R\),
* provincia pa \(R\),
* comune pa \(R\),
* latitudine pa \(R\),
* longitudine pa \(R\),
* codice univoco con prefisso pa (F),
* tipo pa (O),
* nome pa (O).

### Domain channel country
[RACCOMANDATO] La nazione a cui appartiene la pa che emette l'atto. Esempio:

```xml
<category domain="http://albopop.it/specs#channel-category-country">Italia</category>
```

### Domain channel region
[RACCOMANDATO] La regione a cui appartiene la pa che emette l'atto. Esempio:

```xml
<category domain="http://albopop.it/specs#channel-category-region">Sicilia</category>
```

### Domain channel province
[RACCOMANDATO] La provincia a cui appartiene la pa che emette l'atto. Esempio:

```xml
<category domain="http://albopop.it/specs#channel-category-province">Palermo</category>
```

### Domain channel municipality
[RACCOMANDATO] Il comune a cui appartiene la pa che emette l'atto. Esempio:

```xml
<category domain="http://albopop.it/specs#channel-category-municipality">Bagheria</category>
```

### Domain channel latitude
[RACCOMANDATO] La latitudine della pa che emette l'atto. Quella del comune di riferimento in mancanza di una sede specifica. Esempio:

```xml
<category domain="http://albopop.it/specs#channel-category-latitude">38.083333</category>
```

### Domain channel longitude
[RACCOMANDATO] La longitudine della pa che emette l'atto. Quella del comune di riferimento in mancanza di una sede specifica. Esempio:

```xml
<category domain="http://albopop.it/specs#channel-category-longitude">13.5</category>
```

### Domain channel uid
[FACOLTATIVO] L'identificativo univoco della pa che emette l'atto, con un prefisso che ne indica il database di riferimento.
Per un comune valgono per esempio i codici ISTAT. Esempio:

```xml
<category domain="http://albopop.it/specs#channel-category-uid">istat:082006</category>
```

### Domain channel type
[OBBLIGATORIO] Il tipo di pa che emette l'atto. Esempio:

```xml
<category domain="http://albopop.it/specs#channel-category-type">Comune</category>
```

### Domain channel name
[OBBLIGATORIO] Il nome della pa che emette l'atto. Esempio:

```xml
<category domain="http://albopop.it/specs#channel-category-name">Comune di Bagheria</category>
```

# Tag item
Sono gli elementi che rappresentano un singolo atto dell'albo pretorio. Possono essere zero (feed vuoto) o più, fino a 25, in ordine inverso di data e orario di pubblicazione.

```xml
<item>[...]</item>
```

## Tag title
[OBBLIGATORIO] Il titolo dell'atto, così come riportato dalla pagina dedicata dell'albo pretorio. Si sconsiglia ogni intervento sul testo, come riduzione in lettere minuscole, a parte l'eliminazione di spazi e tabulazioni consecutivi e/o agli estremi della stringa.

```xml
<title>[...]</title>
```

## Tag link
[OBBLIGATORIO] L'URL diretto alla pagina ufficiale dell'atto. Non a un documento (un file pdf, per esempio, vedi il tag [enclosure](#item-enclosure)), ma a una pagina web. E possibilmente diretto alla pagina, non attraverso un redirect, uno short url o un feedproxy.

```xml
<link>[...]</link>
```

## Tag description
[OBBLIGATORIO] L'excerpt dell'atto, così come riportato in forma sintetica nella pagina ufficiale. Se mancante, una copia del titolo. Si sconsiglia ogni intervento sul testo, come riduzione in lettere minuscole, a parte l'eliminazione di spazi e tabulazioni consecutivi e/o agli estremi della stringa. Può contenere HTML, ma in tal caso deve essere racchiuso in un tag `<![CDATA[]]>` (vedi [CDATA Section](https://www.w3.org/TR/REC-xml/#sec-cdata-sect)).

```xml
<description><![CDATA[...]]></description>
```

## Tag pubDate
[OBBLIGATORIO] Data e orario ufficiali di pubblicazione dell'atto all'interno dell'Albo Pretorio, in formato conforme alle specifiche [RFC 822](https://www.w3.org/Protocols/rfc822/#z28). Deve essere indipendente da data e orario di scraping (che sono riportati nel [tag pubDate del tag channel](#channel-pubdate)).

```xml
<pubDate>[...]</pubDate>
```

## Tag guid
[OBBLIGATORIO] Identificativo unico *universale* dell'atto. Non può essere il semplice id dell'atto (che non è universalmente unico), generalmente è uguale all'URL diretto alla pagina ufficiale (in questo caso deve contenere l'attributo `isPermalink`).

```xml
<guid isPermaLink="true">[...]</guid>
```

## Tag item category
Devono essere uno (codice univoco) o più e contenere il maggior numero possibile delle seguenti informazioni, suddivise in obbligatorie (O), raccomandate \(R\), facoltative (F):

* nazione atto (F),
* regione atto (F),
* provincia atto (F),
* comune atto (F),
* latitudine atto (F),
* longitudine atto (F),
* codice univoco con prefisso atto (O),
* tipo atto \(R\),
* data di pubblicazione dell'atto \(R\),
* data di scadenza dell'atto \(R\),
* data di emissione dell'atto (F),
* data di inizio esecuzione dell'atto (F),
* capitolo dell'atto \(R\),
* unità organizzativa responsabile dell'atto \(R\),
* importo complessivo dell'atto \(R\),
* valuta in cui è espresso l'importo \(R\),
* annotazioni a corredo dell'atto (F).

### Domain item country
[FACOLTATIVO] La nazione di riferimento dell'atto. Se presente, ha precedenza sulla corrispondente categoria del *channel*. Esempio:

```xml
<category domain="http://albopop.it/specs#item-category-country">Italia</category>
```

### Domain item region
[FACOLTATIVO] La regione di riferimento dell'atto. Se presente, ha precedenza sulla corrispondente categoria del *channel*. Esempio:

```xml
<category domain="http://albopop.it/specs#item-category-region">Sicilia</category>
```

### Domain item province
[FACOLTATIVO] La provincia di riferimento dell'atto. Se presente, ha precedenza sulla corrispondente categoria del *channel*. Esempio:

```xml
<category domain="http://albopop.it/specs#item-category-province">Palermo</category>
```

### Domain item municipality
[FACOLTATIVO] Il comune di riferimento dell'atto. Se presente, ha precedenza sulla corrispondente categoria del *channel*. Esempio:

```xml
<category domain="http://albopop.it/specs#item-category-municipality">Bagheria</category>
```

### Domain item latitude
[FACOLTATIVO] La latitudine di riferimento dell'atto. Se presente, ha precedenza sulla corrispondente categoria del *channel*. Quella del comune di riferimento in mancanza di una sede specifica. Esempio:

```xml
<category domain="http://albopop.it/specs#item-category-latitude">38.083333</category>
```

### Domain item longitude
[FACOLTATIVO] La longitudine di riferimento dell'atto. Se presente, ha precedenza sulla corrispondente categoria del *channel*. Quella del comune di riferimento in mancanza di una sede specifica. Esempio:

```xml
<category domain="http://albopop.it/specs#item-category-longitude">13.5</category>
```

### Domain item uid
[OBBLIGATORIO] L'identificativo numerico univoco dell'atto, univoco all'interno dell'Albo Pretorio in oggetto, nella forma `[anno]/[numero atto]`. Esempio:

```xml
<category domain="http://albopop.it/specs#item-category-uid">2016/15</category>
```

### Domain item type
[RACCOMANDATO] La tipologia di atto. Esempio:

```xml
<category domain="http://albopop.it/specs#item-category-type">Determina</category>
```

### Domain item pubStart
[RACCOMANDATO] La data di pubblicazione dell'atto, in formato conforme alle specifiche [RFC 822](https://www.w3.org/Protocols/rfc822/#z28). Se assente coincide con la [data di pubblicazione nell'albo pretorio](#item-pubdate). Esempio:

```xml
<category domain="http://albopop.it/specs#item-category-pubStart">Wed, 14 Sep 2016 10:00:00 +000</category>
```

### Domain item pubEnd
[RACCOMANDATO] La data di scadenza dell'atto, in formato conforme alle specifiche [RFC 822](https://www.w3.org/Protocols/rfc822/#z28). Se assente si intende uguale a 15 giorni dopo [pubStart](#item-category-pubStart). Esempio:

```xml
<category domain="http://albopop.it/specs#item-category-pubEnd">Wed, 29 Sep 2016 10:00:00 +000</category>
```

### Domain item relStart
[FACOLTATIVO] La data di emissione dell'atto, in formato conforme alle specifiche [RFC 822](https://www.w3.org/Protocols/rfc822/#z28). Esempio:

```xml
<category domain="http://albopop.it/specs#item-category-relStart">Wed, 14 Sep 2016 10:00:00 +000</category>
```

### Domain item exeStart
[FACOLTATIVO] La data di inizio esecuzione dell'atto, in formato conforme alle specifiche [RFC 822](https://www.w3.org/Protocols/rfc822/#z28). Esempio:

```xml
<category domain="http://albopop.it/specs#item-category-exeStart">Wed, 14 Sep 2016 10:00:00 +000</category>
```

### Domain item chapter
[RACCOMANDATO] Il capitolo dell'atto. Esempio:

```xml
<category domain="http://albopop.it/specs#item-category-chapter">3</category>
```

### Domain item unit
[RACCOMANDATO] Unità organizzativa responsabile dell'atto. Esempio:

```xml
<category domain="http://albopop.it/specs#item-category-unit">Area Servizi Demografici</category>
```

### Domain item amount
[RACCOMANDATO] L'importo complessivo dell'atto, espresso nella [valuta indicata](#item-category-currency) (in euro, se non diversamente specificato). Esempio:

```xml
<category domain="http://albopop.it/specs#item-category-amount">2000.00</category>
```

### Domain item currency
[RACCOMANDATO] La valuta in cui è espresso l'[importo](#item-category-amount). Se assente si intende espresso in euro. Esempio:

```xml
<category domain="http://albopop.it/specs#item-category-currency">€</category>
```

### Domain item annotation
[FACOLTATIVO] Annotazioni a corredo dell'atto. Esempio:

```xml
<category domain="http://albopop.it/specs#item-category-annotation">[...]</category>
```

## Tag enclosure
[RACCOMANDATO] Uno o più allegati con l'URL diretto all'atto integrale, generalmente un file pdf. Esempio:

```xml
<enclosure url="[...]" length="[...]" type="application/pdf" />
```

# Esempio completo
Qui di seguito è riportato un esempio completo di feed con un solo elemento a scopo dimostrativo.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<rss xmlns:creativeCommons="http://cyber.law.harvard.edu/rss/creativeCommonsRssModule.html" xmlns:xhtml="http://www.w3.org/1999/xhtml" version="2.0">
 <channel>
 
  <title>AlboPOP - Comune - Bagheria</title>
  <link>https://script.google.com/macros/s/AKfycbxiqe9sZ7Y1yT8dm3diccl0EBhGAQ5ZF60Stq8SgM4qSIabfeA/exec</link>
  <description>*non ufficiale* RSS feed dell'Albo Pretorio del Comune di Bagheria</description>
  <language>it</language>
  <pubDate>Wed, 14 Sep 2016 10:30:00 +000</pubDate>
  <webMaster>john@smith.com (John Smith)</webMaster>
  <docs>http://albopop.it/comune/bagheria/</docs>
  <copyright>Copyright 2016 Comune di Bagheria</copyright>
  <xhtml:meta name="robots" content="noindex" />
  
  <category domain="http://albopop.it/specs#channel-category-country">Italia</category>
  <category domain="http://albopop.it/specs#channel-category-region">Sicilia</category>
  <category domain="http://albopop.it/specs#channel-category-province">Palermo</category>
  <category domain="http://albopop.it/specs#channel-category-municipality">Bagheria</category>
  <category domain="http://albopop.it/specs#channel-category-latitude">38.083333</category>
  <category domain="http://albopop.it/specs#channel-category-longitude">13.5</category>
  <category domain="http://albopop.it/specs#channel-category-type">Comune</category>
  <category domain="http://albopop.it/specs#channel-category-name">Comune di Bagheria</category>
  <category domain="http://albopop.it/specs#channel-category-uid">istat:082006</category>

  <item>
  
   <title>Servizio trasporto alunni Aspra-Bagheria</title>
   <link>http://comune.bagheria.pa.it/albo-pretorio/albo-pretorio-online/?ap_id=26006&ap_show=detail</link>
   <description><![CDATA[Servizio trasporto alunni Aspra-Bagheria]]></description>
   <pubDate>Wed, 14 Sep 2016 10:00:00 +000</pubDate>
   <guid isPermaLink="true">http://comune.bagheria.pa.it/albo-pretorio/albo-pretorio-online/?ap_id=26006&ap_show=detail</guid>
   
   <category domain="http://albopop.it/specs#item-category-uid">2016/117</category>
   <category domain="http://albopop.it/specs#item-category-type">Determina</category>
   <category domain="http://albopop.it/specs#item-category-pubStart">Wed, 14 Sep 2016 10:00:00 +000</category>
   <category domain="http://albopop.it/specs#item-category-pubEnd">Wed, 29 Sep 2016 10:00:00 +000</category>
   <category domain="http://albopop.it/specs#item-category-relStart">Wed, 14 Sep 2016 10:00:00 +000</category>
   <category domain="http://albopop.it/specs#item-category-exeStart">Wed, 15 Sep 2016 10:00:00 +000</category>
   <category domain="http://albopop.it/specs#item-category-chapter">3</category>
   <category domain="http://albopop.it/specs#item-category-unit">Area Servizi Demografici</category>
   <category domain="http://albopop.it/specs#item-category-amount">2000.00</category>
   <category domain="http://albopop.it/specs#item-category-currency">€</category>
   <category domain="http://albopop.it/specs#item-category-annotation">Per info contattare info@example.com</category>
   
   <enclosure url="http://comune.bagheria.pa.it/wp-content/plugins/wp-albopretorio/download.ws.php?download=deed_doc&id=26976" length="222849" type="application/pdf" />
   
  </item>
  
 </channel>
</rss>
```
